class Empregado

    attr_reader :nome, :salario

    def initialize(nome:nil, salario: nil, pagamento:nil)
        @nome = nome
        @salario = salario
        @pagamento = pagamento
    end

    def salario=(novo_salario)
        @salario = novo_salario
        @pagamento.atualizar(self)
    end

end
